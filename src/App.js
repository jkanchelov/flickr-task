import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import $ from 'jquery'
import CardsHolder from './components/CardsHolder'
import logo from './logo.svg';
import './css/App.css';

class App extends Component {
  state = {
    flickrData: []
  };

  async getFlickrData() {
    const flickrApiPoint = "https://api.flickr.com/services/feeds/photos_public.gne";

    try {
      // let flickrData = await (await fetch(flickrApiPoint,{
      //   mode: "no-cors"
      // }))
      
      // console.log(flickrData);
      // this.setState({ flickrData });

      $.ajax({
        url: flickrApiPoint,
        dataType: 'jsonp',
        data: { "format": "json" },
        //tell YQL what we want and that we want JSON
        //work with the response
        success: function (data) {
          console.log(data); //formatted JSON data
        }
      });
    }
    catch (e) {
      console.log(e);
    }
  }

  componentDidMount() {
    this.getFlickrData();
  }

  render() {
    return (
      <MuiThemeProvider>
        <div className="App">
          <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <h1 className="App-title">Flickr task</h1>
          </header>

          <CardsHolder flickrData={this.state.flickrData} />
        </div>
      </MuiThemeProvider>
    );
  }
}

export default App;
