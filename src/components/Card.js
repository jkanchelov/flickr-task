import React, { Component } from 'react';
import { Card as MaterialCard, CardMedia, CardTitle, CardText } from 'material-ui/Card';
import $ from 'jquery'
import '../css/components/card.css';


class Card extends Component {
    render() {
        let descriptionString = `<p><a href="https://www.flickr.com/people/163756533@N02/">KVentz</a> posted a photo:</p> <p><a href="https://www.flickr.com/photos/163756533@N02/26360011147/" title="Свято-Успенский мужской монастырь. Бахчисарай, Крым"><img src="https://farm1.staticflickr.com/887/26360011147_f7910d8dd4_m.jpg" width="240" height="160" alt="Свято-Успенский мужской монастырь. Бахчисарай, Крым" /></a></p> <p>Что-то у меня в ленте сплошные механизмы и подземелья. Давайте посмотрим на что-то более воздушное…<br /> <br /> Свято-Успенский мужской монастырь. Бахчисарай, Крым.<br /> <br /> Свято-Успенский пещерный мужской монастырь был основан византийскими монахами-иконопочитателями не позднее VIII века. Он расположен в урочище Мариам-Дере (Ущелье Марии) вблизи Бахчисарая. Особенность его в том, что значительная часть его постоек вырублена в скалах, в том числе несколько церквей.<br /> <br /> #Крым #Бахчисарай #природа #пейзаж #горы #небо #монастырь #храм #лестница #Crimea #Bakhchysarai #nature #landscape #mountains #sky #monastery #temple</p>`;
        let htmlElem = $(descriptionString);

        let title = <div className="cardTitle" dangerouslySetInnerHTML={{ __html: `<a href="https://www.flickr.com/people/157124895@N07/">Свято-Успенский мужской монастырь. Бахчисарай, Крым</a>` }}></div>;
        let author =  <div className="cardAuthor" dangerouslySetInnerHTML={{ __html: `by <a href="https://www.flickr.com/people/157124895@N07/">yesidneira</a>` }}></div>
        
        return (
            <MaterialCard className={"card"}>
                <CardMedia>
                    <img src="https://farm1.staticflickr.com/887/26360011147_f7910d8dd4_m.jpg" alt="" />
                </CardMedia>
                <CardTitle  title={title} subtitle={author}/>
                <CardText>
                    <div style={{ color: "#424242"}} dangerouslySetInnerHTML={{__html: htmlElem[4].innerHTML}}></div>
                </CardText>
            </MaterialCard>
        )
    }
}

export default Card;