import $ from 'jquery'
import React, { Component } from 'react';
import Card from './Card'

class CardsHolder extends Component {
    render() {
        return this.props.flickrData.map(data => {
            let descriptionString = $(data.description)[4].innerHTML.filter(word => word.startsWith('#'));
            let title = data.title;
            let author = data.author
            let img = data.media.m;

            return <Card props={{
                descriptionString,
                title,
                author,
                img
            }}/>
        })
    }
}

export default CardsHolder;